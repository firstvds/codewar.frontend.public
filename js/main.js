/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var game = {};
(function ($) {

	game = {
		host: "https://codewar.firstvds.ru/",
		quest_session_url: "/quest/quest_get_session_id/", // url получения id сессии у друпала
		area: d3,
		play_status: false,
		current_step: 1,
		spinner: $('<span>').addClass('glyphicon glyphicon-cog isp-quest-spin'),
		members: {
			nexa: {},
			john: {}
		},
		init: function () {
			var gm = this;
			try {
				this.editor = ace.edit("editor");
				this.editor.setTheme("ace/theme/twilight");
				this.editor.session.setMode("ace/mode/javascript");
				// для удобства пользователя сделаем сохранение его кода в localstorage
				this.editor.on("input", function () {
					localStorage.setItem('codewar_code', gm.editor.getValue());
				});
				if (localStorage.getItem('codewar_code')) {
					gm.editor.setValue(localStorage.getItem('codewar_code'));
				}
			} catch (e) {
				console.log("ace editor error");
			}
			// сходим к апи игры и узнаем начальные параметры
			this.getGameEnviroment().then(function (enviroment) {
				// затем инициализируем остальные сервисы
				gm.enviroment = enviroment;
				gm.printGameScale(enviroment); // нарисуем сетку
				gm.printGameBegin(enviroment); // нарисуем Джона, Нексу и зараженные клетки
				gm.bindControlElements(); // привяжем к элементам управления события
				//gm.startWar(gm.editor.getValue());
			});
		},
		getGameEnviroment: function () {
			var d = $.Deferred();
			$.ajax({
				url: this.host + 'enviroment',
				crossDomain: true
			}).then(function (data) {
				d.resolve(data);
			});
			return d.promise();
		},
		printGameScale: function (enviroment) {
			d3
				.select('#codewar__scale')
				.selectAll("*").remove();
			var area = d3
				.select('#codewar__scale')
				.append('svg')
				.attr('class', 'chart_area')
				.attr('width', 500)
				.attr('height', 500)
				.attr("viewBox", "0 0 " + (enviroment.scale.x + 1) + " " + (enviroment.scale.y + 1))
				;
			this.area = area;

			this.area
				.insert("rect")
				.classed("codewar__arena", true)
				.attr("x", 0.5)
				.attr("y", 0.5)
				.attr("width", 10)
				.attr("height", 10);

			for (var i = 0; i < enviroment.scale.x + 1; i++) {
				this.area
					.append("line")
					.classed("codewar__grid", true)
					.attr("x1", i + 0.5)
					.attr("x2", i + 0.5)
					.attr("y1", 0 + 0.5)
					.attr("y2", enviroment.scale.y + 0.5)
					;
			}
			for (var j = 0; j < enviroment.scale.y + 1; j++) {
				this.area
					.append("line")
					.classed("codewar__grid", true)
					.attr("y1", j + 0.5)
					.attr("y2", j + 0.5)
					.attr("x1", 0 + 0.5)
					.attr("x2", enviroment.scale.x + 0.5)
					;
			}
		},
		printGameBegin: function (enviroment) {
			this.members.nexa.svg = this.area
				.append("svg:image")
				.attr('x', enviroment.nexa.beginPosition.x - 0.25)
				.attr('y', enviroment.nexa.beginPosition.y - 0.25)
				.attr('width', 0.5)
				.attr('height', 0.5)
				.attr("xlink:href", "/sites/all/modules/custom/isp_quest/game/images/ai.png")
				.classed("codewar__nexa", true);

			this.members.john.svg = this.area
				.append("svg:image")
				.attr('x', enviroment.john.beginPosition.x - 0.25)
				.attr('y', enviroment.john.beginPosition.y - 0.25)
				.attr('width', 0.5)
				.attr('height', 0.5)
				.attr("xlink:href", "/sites/all/modules/custom/isp_quest/game/images/user.png")
				.classed("codewar__john", true);


			for (var i in enviroment.infection) {
				this.infectCell(enviroment.infection[i].x, enviroment.infection[i].y);
			}

			this.infectCell(enviroment.nexa.beginPosition.x, enviroment.nexa.beginPosition.y);
		},
		restartGame: function () {
			this.timeline = [];
			this.play_status = false;
			this.current_step = 0;
			this.printGameScale(this.enviroment);
			this.printGameBegin(this.enviroment);
			this.play_status = true;
			this.startWar(this.editor.getValue());
		},
		startWar: function (code) {
			var th = this;
			this.getQuestSession().then(function (session_data) {

				th.getWarResult(code, session_data).then(function (response) {

					th.timeline = response.timeline;
					th.result = response.result;
					th.printWarResult(response.result);
					th.gameStep(1);
				});
			});
		},
		getQuestSession: function () {

			var d = $.Deferred();
			$.ajax({
				url: this.quest_session_url,
				crossDomain: true
			}).then(function (data) {
				if (data && data.session) {
					d.resolve(data);
					
				}
			});
			return d.promise();
		},
		getWarResult: function (code, session_data) {

			var d = $.Deferred();
			var $button = $('.btn[data-control=send-code]');
			var button_text = $button.html();
			$button.html(this.spinner.clone());
			$.ajax({
				url: this.host + 'result',
				data: {
					code: code,
					session: session_data.session
				},
				crossDomain: true
			}).then(function (timeline) {
				$button.html(button_text);
				d.resolve(timeline);
			});
			return d.promise();
		},
		printWarResult: function (result) {
			$('.js-codewar__result[data-result]').each(function () {
				switch ($(this).data('result')) {
					case 'title':
						$(this).removeClass("success").removeClass("danger");
						if (result.win) {
							$(this).html("Вы очистили поле за " + result['steps'] + " ходов");
							$(this).addClass("success");
						} else {
							if (result.error) {
								$(this).html(result.error);
							} else {
								$(this).html("Вам не удалось справиться с Нексой :( <br>Попробуйте еще раз!");
							}
							$(this).addClass("danger");
						}
						break;
					default:
						$(this).html(result[$(this).data('result')]);
						break;
				}

			});
		},
		gameStep: function (step, one_step) {
			var th = this;
			if (step in th.timeline) {
				if (this.play_status || one_step) {

					this.current_step = step;
					this.members.nexa.svg
						.transition()
						.attr("x", th.timeline[step].nexa.position.x - 0.25)
						.attr("y", th.timeline[step].nexa.position.y - 0.25)
						.duration(200)
						.on("start", function () {

						})
						.on("end", function () {
							th.infect(step);
							//if (step < th.enviroment.maxSteps) th.gameStep(step + 1);

							th.members.john.svg
								.transition()
								.attr("x", th.timeline[step].john.position.x - 0.25)
								.attr("y", th.timeline[step].john.position.y - 0.25)
								.duration(200)
								.on("start", function () {

								})
								.on("end", function () {
									//th.infect(step);
									if (step < th.enviroment.maxSteps)
										th.gameStep(step + 1);
								});
						});
				}
			}
		},
		infect: function (step) {
			for (var i = 1; i < this.enviroment.scale.x + 1; i++) {
				for (var j = 1; j < this.enviroment.scale.y + 1; j++) {
					var yes = this.timeline[step].infection.filter(function (obj) {
						return obj.x == i && obj.y == j;
					});
					if (yes.length) {
						this.infectCell(i, j);
					} else {
						$(".codewar__infected[x_=" + parseInt(i) + "][y_=" + parseInt(j) + "]").remove();
					}
				}
			}

		},
		infectCell: function (x, y) {
			this.area
				.insert("rect", ".codewar__nexa")
				.classed("codewar__infected", true)
				.attr("x", x - 0.5)
				.attr("y", y - 0.5)
				.attr("x_", x)
				.attr("y_", y)
				.attr("z-index", 1)
				.attr("width", 1)
				.attr("height", 1);
		},
		sendCode: function () {
			var code = this.editor.getValue();
			console.log(code);
			this.restartGame();
		},
		pause: function () {
			this.play_status = false;
		},
		play: function () {
			this.play_status = true;
			this.gameStep(this.current_step);
		},
		next: function () {
			this.play_status = false;
			this.gameStep(this.current_step + 1, true);
		},
		prev: function () {
			this.play_status = false;
			this.gameStep(this.current_step - 1, true);
		},
		start: function () {
			this.play_status = false;
			this.gameStep(0, true);
		},
		end: function () {
			this.play_status = false;
			console.log(this.enviroment);
			this.gameStep(this.enviroment.maxSteps - 1, true);
		},
		bindControlElements: function () {
			var th = this;
			$('[data-control]', $('.codewar')).each(function () {
				$(this).on('click', function (e) {
					e.preventDefault();
					switch ($(this).data('control')) {
						case 'play':
							th.play();
							break;
						case 'pause':
							th.pause();
							break;
						case 'prev':
							th.prev();
							break;
						case 'next':
							th.next();
							break;
						case 'start':
							th.start();
							break;
						case 'end':
							th.end();
							break;
						case 'send-code':
							th.sendCode();
							break;
					}
				});
			});
		}
	}

	$(document).ready(function () {
		//game.init();
	});
})(jQuery)